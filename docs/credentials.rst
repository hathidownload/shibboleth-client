:mod:`~arcs.shibboleth.client.credentials` -- password management
=================================================================

.. _ref-credentials

.. module:: arcs.shibboleth.client.credentials
.. moduleauthor:: Russell Sim <russell@vpac.org>


:class:`SimpleCredentialManager` Objects
----------------------------------
.. autoclass:: SimpleCredentialManager
   :members:
   :undoc-members:


:class:`CredentialManager` Objects
----------------------------------
.. autoclass:: CredentialManager
   :members:
   :undoc-members:


:class:`Idp` Objects
--------------------
.. autoclass:: Idp
   :members:
   :undoc-members:

