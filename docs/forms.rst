:mod:`~arcs.shibboleth.client.forms` -- shibboleth form handlers
================================================================

.. _ref-forms

.. module:: arcs.shibboleth.client.forms
.. moduleauthor:: Russell Sim <russell@vpac.org>


:class:`FormParser` Objects
---------------------------
.. autoclass:: FormParser
   :members:
   :undoc-members:


:class:`FormHandler` Objects
----------------------------
.. autoclass:: FormHandler
   :members:
   :undoc-members:


:class:`DS` Objects
-------------------
.. autoclass:: DS
   :members:
   :undoc-members:


:class:`WAYF` Objects
---------------------
.. autoclass:: WAYF
   :members:
   :undoc-members:


:class:`IdPFormLogin` Objects
-----------------------------
.. autoclass:: IdPFormLogin
   :members:
   :undoc-members:


:class:`CASFormLogin` Objects
-----------------------------
.. autoclass:: CASFormLogin
   :members:
   :undoc-members:


:class:`IdPSPForm` Objects
--------------------------
.. autoclass:: IdPSPForm
   :members:
   :undoc-members:


:class:`IdPSPFormRelayState` Objects
------------------------------------
.. autoclass:: IdPSPFormRelayState
   :members:
   :undoc-members:


